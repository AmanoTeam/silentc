import asyncio
import html
import os
import sys
from functools import partial
from termcolor import cprint

import config
from config import logs_chat

loop = asyncio.get_event_loop()

async def run_client(client):
	try:
		await client.start()
	except AttributeError as e:
		if 'key' in str(e).lower():
			return cprint(str(e).split('. ')[0]+f". Run '{os.path.basename(sys.executable)} setup.py' first.", 'red')
		raise e
		
	client.set_parse_mode('html')
	loop.call_later(5, partial(asyncio.run_coroutine_threadsafe, alert_init(client), loop))
	await client.idle()

async def alert_init(client):
	plugins_count = sum(
		[len(group) for group in client.dispatcher.groups.values()]
	)
	started_text = f"""👾 Bot started!
🖇 Loaded <code>{plugins_count}</code> plugin{'s' if plugins_count != 1 else ''}.

🏷 <b>app_version</b>: <code>{html.escape(client.app_version)}</code>
🛸 <b>device_model</b>: <code>{html.escape(client.device_model)}</code>
💻 <b>system_version</b>: <code>{html.escape(client.system_version)}</code>"""
	try:
		await client.send_message(logs_chat, started_text)
	except:
		pass
		
loop.run_until_complete(run_client(config.app))

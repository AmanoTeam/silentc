import asyncio

from config import langs
from database import User
from pyrogram import Client, Filters
from termcolor import cprint
from utils import ikb

@Client.on_message(Filters.command('menu'))
async def onmenu(client, message):
	lang = message.lang
	await message.reply(lang.menu_text)
import asyncio

from config import langs
from database import User
from pyrogram import Client, Filters
from termcolor import cprint
from utils import ikb

@Client.on_message(Filters.command('start'))
async def onstart(client, message):
	lang = message.lang
	keyboard = ikb([
		[('Ajuda', 'help'), ('Sobre', 'about')],
		[('Canais', 'channels')]
	])
	await message.reply(lang.start_text, reply_markup=keyboard)
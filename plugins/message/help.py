import asyncio

from config import langs
from database import User
from pyrogram import Client, Filters
from termcolor import cprint
from utils import ikb

@Client.on_message(Filters.command('help'))
async def onhelp(client, message):
	lang = message.lang
	await message.reply(lang.help_text(article_link=lang.faq, group_link=lang.support_group))
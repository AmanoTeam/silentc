import asyncio

from config import langs
from database import User
from pyrogram import Client, Filters
from termcolor import cprint
from utils import ikb

@Client.on_message(Filters.command('settings'))
async def onsettings(client, message):
	lang = message.lang
	await message.reply(lang.settings_info)
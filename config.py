import asyncio
import json
import os
from base64 import b64decode, b64encode

import dotenv
import pyrogram_mod
from pyrogram import Client

from utils import Langs

if os.path.exists('.env'):
	dotenv.load_dotenv()

for required_env in ['LOGS_CHAT', 'SUDOERS_LIST', 'DATABASE_URL']:
	if required_env not in os.environ:
		raise AttributeError(f'Missing required env variable: {required_env}')
	if not os.getenv(required_env):
		raise ValueError(f'Invalid value for required env variable {required_env}')

# Extra **kwargs for creating pyrogram.Client
config = json.loads(
	b64decode(
		os.getenv('PYROGRAM_CONFIG', 'e30=').encode() or b'{}'
	).decode()
)
app = Client(os.getenv('PYROGRAM_SESSION') or 'bot', plugins={"root":"plugins"}, **config)

langs = Langs(
	en='./strings/strings_en.json',
	ptbr='./strings/strings_ptbr.json'
)

def int_if_possible(value):
	try:
		return int(value)
	except:
		return value
	
logs_chat = int_if_possible(os.getenv('LOGS_CHAT'))
sudoers = list(map(int_if_possible, os.getenv('SUDOERS_LIST').split(' ')))
